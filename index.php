<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vallarta Lifestyles Media Group | Grupo Editorial y Multimedia</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    
    <link rel="canonical" href="https://vlmg.mx" />
    <meta name="description" content="Vallarta Lifestyles Media Group es el grupo editorial de mayor prestigio en Puerto Vallarta y cuenta con un amplio portafolio de contenidos multiplataforma."/>
    <meta property="og:locale" content="es_MX" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Vallarta Lifestyles Media Group | Grupo Editorial y Multimedia " />
    <meta property="og:description" content="Vallarta Lifestyles Media Group es el grupo editorial de mayor prestigio en Puerto Vallarta y cuenta con un amplio portafolio de contenidos multiplataforma." />
    <meta property="og:url" content="https://vlmg.mx" />
    <meta property="og:site_name" content="Vallarta Lifestyles Media Group" />
    <meta property="og:image" content="https://vlmg.mx/images/bg-intro-social.png" />
    <meta property="og:image:secure_url" content="https://vlmg.mx/images/bg-intro-social.png" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Vallarta Lifestyles Media Group es el grupo editorial de mayor prestigio en Puerto Vallarta y cuenta con un amplio portafolio de contenidos multiplataforma." />
    <meta name="twitter:title" content="Vallarta Lifestyles Media Group | Grupo Editorial y Multimedia" />
    <meta name="twitter:image" content="https://vlmg.mx/images/bg-intro-social.png" />

    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/vlmg.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Quicksand:300,regular,500,700", "Crimson Text:regular"]
            }
        });
    </script>

    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">
        ! function(o, c) {
            var n = c.documentElement,
                t = " w-mod-";
            n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
        }(window, document);
    </script>

    <!-- FAVICON -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="images/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="images/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="images/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="images/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="images/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="images/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Vallarta Lifestyles" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="images/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="images/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="images/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="images/favicon/mstile-310x310.png" />

    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107594929-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-107594929-1');
    </script>
</head>
<body class="body">

    <div class="section-intro">
        <div class="container">
            <header class="container-padding">
                <div class="intro-logo-container">
                    <a href="/" data-ix="intro" class="intro-logo-link w-inline-block">
                        <img src="images/logo-vlmg.svg" class="intro-logo" alt="Vallarta Lifestyles Media Group" title="Vallarta Lifestyles Media Group">
                    </a>
                </div>
                <div class="intro-content-wrapper">
                    <div class="intro-main-content">
                        <h1 data-ix="intro-title" class="intro-title">Publicaciones de interés,<br>historias que inspiran...</h1>
                        <div data-ix="intro-text" class="intro-text-wrapper">
                            <p class="paragraph">Con más de tres décadas de trayectoria, Vallarta Lifestyles Media Group es el grupo editorial de mayor prestigio en Vallarta · Nayarit. Siempre a la vanguardia, nuestro amplio portafolio de contenidos multiplataforma y experiencia nos coloca como uno de los medios de comunicación más reconocidos de la región.
</p>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    </div>

    <div class="features-options">
        <a href="#" data-ix="zoomin" class="feature-option purple w-inline-block">
            <img src="images/icon-person-white.svg" class="feature-icon" alt="">
            <h2 class="feature-text">Equipo Interdisciplinario y Multicultural</h2>
        </a>
        <a href="#" data-ix="zoomin" class="blue feature-option w-inline-block">
            <img src="images/icon-cube-white.svg" class="feature-icon" alt="">
            <h2 class="feature-text">Innovación</h2>
        </a>
        <a href="#" data-ix="zoomin" class="feature-option w-inline-block">
            <img src="images/icon-heart-white.svg" class="feature-icon" alt="">
            <h2 class="feature-text">Pasión</h2>
        </a>
    </div>

    <div class="features-wrapper">
        <div class="featurec-wrapper-left"></div>
        <div class="features-wrapper-right">
            <div class="feature-selected-wrapper" data-ix="scroll-animation-20">
                <div class="active feature-selected">
                    <img src="images/icon-person-purple.svg" class="feature-selected-icon" alt="">
                    <div class="feature-selected-title">Equipo Interdisciplinario <span class="feature-title-selected-big">y Multicultural</span></div>
                    <p class="feature-selected-text">Conformado por un equipo de trabajo interdisciplinario y multicultural, Vallarta Lifestyles Media Group muestra en sus páginas diferentes causas que celebran la diversidad social y cultural de Vallarta · Nayarit.</p>
                </div>
                <div class="feature-selected">
                    <img src="images/icon-cube-purple.svg" class="feature-selected-icon" alt="">
                    <div class="feature-selected-title"><span class="feature-title-selected-big">Innovación</span></div>
                    <p class="feature-selected-text">En una región en constante cambio, nuestro equipo editorial trabaja de manera constante para mantenerse a la vanguardia, siempre con el objetivo de posicionar a Vallarta · Nayarit como uno de los mejores destinos a nivel internacional.</p>
                </div>
                <div class="feature-selected">
                    <img src="images/icon-heart-purple.svg" class="feature-selected-icon" alt="">
                    <div class="feature-selected-title"><span class="feature-title-selected-big">Pasión</span></div>
                    <p class="feature-selected-text">El equipo editorial de Vallarta Lifestyles Media Group está comprometido en ofrecer publicaciones de calidad, atractivas y diferentes; celebrando la diversidad social y cultural que converge en Vallarta · Nayarit.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="publications-intro-img"></div>
    <div data-ix="scroll-animation-40" class="publications-intro-wrapper">
        <div class="publications-intro-wrapper-inner">
            <h2 class="publications-intro-title">
                NUESTRAS<span class="publications-intro-title-small">PUBLICACIONES</span>
            </h2>
        </div>
    </div>

    <div class="publications flex-height-fix">
        <div class="publications-list w-clearfix">
            <a href="http://vallartarealestateguide.com" target="_blank" class="publication-item-link w-inline-block">
                <img src="images/logo-reg.svg" class="publication-item-img" alt="Real Estate Guide Colored Logo" title="Real Estate Guide Colored Logo">
            </a>
            <a href="https://vallartalifestyles.com" target="_blank" class="enabled publication-item-link w-inline-block">
                <img src="images/logo-lifestyles.svg" class="publication-item-img" alt="Lifestyles Colored Logo" title="Lifestyles Colored Logo">
            </a>
            <a href="http://octomedia.mx" target="_blank" class="publication-item-link w-inline-block">
                <img src="images/logo-portico.svg" class="publication-item-img" alt="Portico Colored Logo" title="Portico Colored Logo">
            </a>
            <a href="http://hoysi.mx" target="_blank" class="publication-item-link w-inline-block">
                <img src="images/logo-hoysi.svg" class="publication-item-img" alt="HoySí Colored Logo" title="HoySí Colored Logo">
            </a>
            <a href="https://vallartanautica.com" target="_blank" class="publication-item-link w-inline-block">
                <img src="images/logo-nautica.svg" class="publication-item-img" alt="Vallarta Nautica Colored Logo" title="Vallarta Nautica Colored Logo">
            </a>
            <a href="http://octomedia.mx" target="_blank" class="publication-item-link w-inline-block">
                <img src="images/logo-map.svg" class="publication-item-img" alt="Tourist Map Colored Logo" title="Tourist Map Colored Logo">
            </a>
        </div>
        <div class="publications-wrapper">
            <div class="reg publication-content-item">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>Ante la alta demanda de propiedades en Vallarta · Nayarit y sus alrededores, la industria de los bienes raíces local se ha consolidado como una de las más fuertes de México. Vallarta Real Estate Guide es un referente para todos aquellos que aportan al sector y que desean conocer las últimas tendencias inmobiliarias, así como nuevos desarrollos y opiniones de personalidades que encabezan exitosas agencias.</p>
                        <a target="_blank" href="http://vallartarealestateguide.com" class="publication-link">www.vallartarealestateguide.com</a>
                    </div>
                </div>
                <div class="reg publication-img"></div>
            </div>
            <div class="lifestyles publication-content-item active">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>Caracterizada por su contenido y diseño de gran calidad, esta publicación siempre a la vanguardia, ha presenciado y plasmado el desarrollo de la región por más de 30 años. Vallarta Lifestyles es una celebración a los diversos microdestinos y estilos de vida que convergen en la región, así como un referente para aquellos que buscan aprovechar al máximo lo que Vallarta · Nayarit tiene para ofrecer.</p>
                        <a target="_blank" href="https://vallartalifestyles.com" class="publication-link">www.vallartalifestyles.com</a>
                    </div>
                </div>
                <div class="lifestyles publication-img"></div>
            </div>
            <div class="portico publication-content-item">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>El éxito en la oferta de desarrollos y residencias premier, así como el entretenimiento de alta gama que ofrece Vallarta · Nayarit, lo ha convertido en un lugar que las élites quieren vivir y experimentar al máximo. Pórtico Luxury Condo Living es la guía definitiva para el sibarita y los amantes de lo más selecto que buscan abrazar lo mejor que el estilo de vida en el Pacífico mexicano puede ofrecer.</p>
                    </div>
                </div>
                <div class="portico publication-img"></div>
            </div>
            <div class="hoysi publication-content-item">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>HoySí · La Revista de Vallarta y su Gente es el perfecto espejo donde se pueden reflejar los residentes. Una publicación con enfoque 100 % local que plasma la inquietud y búsqueda de los pequeños placeres de la vida relajada en la bahía, sin perder la perspectiva de responsabilidad y participación de una ciudadanía siempre comprometida con las causas sociales, así como con el desarrollo del lugar en el que viven, sueñan y tanto aman.</p>
                        <a target="_blank" href="http://hoysi.mx" class="publication-link">www.hoysi.mx</a>
                    </div>
                </div>
                <div class="hoysi publication-img"></div>
            </div>
            <div class="nautica publication-content-item">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>Vallarta · Nayarit se ubica en un envidiable enclave natural a orillas del Pacífico mexicano, donde predominan sus interminables playas y aguas tranquilas que invitan a recorrer la costa. Vallarta Nautica contiene información de suma importancia para los aficionados náuticos y es la única guía de puerto del destino que está diseñada para aquellos que saben que la vida en el mar no es solo una gran afición, sino todo un estilo de vida.</p>
                        <a target="_blank" href="https://vallartanautica.com" class="publication-link">www.vallartanautica.com</a>
                    </div>
                </div>
                <div class="nautica publication-img"></div>
            </div>
            <div class="map publication-content-item">
                <div class="publication-data">
                    <div class="publication-data-flex-item">
                        <p>Miles de visitantes llegan a Vallarta · Nayarit buscando aprovechar al máximo cada uno de los recorridos, paisajes, sabores, experiencias, eventos y lugares emblemáticos. Tourist Map es una referencia práctica y con carácter de souvenir que los visitantes pueden utilizar para conocer la ciudad mientras caminan cómodamente entre sus tranquilas calles.</p>
                    </div>
                </div>
                <div class="map publication-img"></div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="container-padding">
                <div class="footer-forms">
                    <div class="footer-form-contact">
                        <div class="form-block">
                            <form method="post" action="contact.php" id="contact_form">
                                <div class="footer-form-title">Contáctanos...</div>
                                <input type="text" class="form-input w-input" name="name" placeholder="Nombre" required="">
                                <div class="form-input-flex">
                                    <input type="text" class="form-input w-input" name="phone" placeholder="Teléfono" required="">
                                    <div class="form-flex-separator"></div>
                                    <input type="email" class="form-input w-input" name="email" placeholder="Email" required="">
                                </div>
                                <input type="hidden" name="cmd" class="cmd" value="">
                                <textarea id="field" name="message" maxlength="5000" placeholder="Mensaje..." class="form-textarea w-input" required="required"></textarea>
                                <input type="submit" value="enviar" class="form-footer-submit w-button">
                            </form>
                            <div class="w-form-done">
                                <div>Hemos recibido su mensaje, lo contactaremos en breve.</div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-form-newsletter">
                        <div class="form-block">
                            <form id="newsletter_form" method="post" action="mc_subscribe.php">
                                <div class="footer-form-title right">Suscríbete a nuestro newsletter:</div>
                                <div class="form-subscribe-flex">
                                    <input type="hidden" name="f_lang" value="en" />
                                    <input type="hidden" id="f_user" name="f_user" value="" />
                                    <input type="email" class="form-input w-input" name="f_email" placeholder="Email" required="">
                                    <input type="submit" value="enviar" class="form-submit-newsletter w-button"></div>
                            </form>
                            <div class="w-form-done">
                                <div>¡Gracias por suscribirte!</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-info-flex">
                    <div class="footer-logo-wrapper">
                        <img src="images/logo-vlmg.svg" class="footer-logo" alt="">
                    </div>

                    <div class="footer-info-separator"></div>

                    <div class="footer-text-wrapper">
                        <div class="footer-text">
                            Timón 1, Marina Vallarta, C.P. 48335, Puerto Vallarta, México<br>
                            <a href="#" class="footer-text-link">+52 (322) 221.0106</a>  |  <a class="footer-text-link">info@vlmg.mx</a>
                        </div>
                        <div class="footer-text-2">
                            Copyright <span class="footer-text-2-light">Vallarta Lifestyles Media Group 2017</span> All Rights Reserved
                        </div>
                    </div>

                    <div class="footer-info-separator"></div>

                    <div class="footer-social-wrapper">
                        <a href="https://www.facebook.com/VallartaLifestyles/" class="footer-social-item w-inline-block" target="_blank">
                            <img src="images/icon-facebook.png" alt="">
                        </a>
                        <a href="https://www.instagram.com/vallartalifestyles/" class="footer-social-item w-inline-block" target="_blank">
                            <img src="images/icon-instagram.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script src="js/scripts.js" type="text/javascript"></script>
</body>

</html>