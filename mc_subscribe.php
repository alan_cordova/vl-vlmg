<?php 

    require 'vendor/autoload.php';
    use Mailgun\Mailgun;
    
    define('MAILCHIMP_API_KEY','4caad0ceb4806bcde07d4d0cffa37cd0-us4');
    define('MAILCHIMP_VLPG_LIST','c1d35ce28d');
    define('MAILCHIMP_GROUPING_ID_PREFERENCES','329');

    require_once 'includes/classes/mailchimp.php';
    require_once 'includes/functions/mailchimp.php';

    $success = true;
    $error = false;

    foreach($_POST as $k=>$v){
        $_POST[$k] = trim($v);
    }


    if(($mail = trim($_POST['f_email']) == '')){
        $success = false;
        $error = true;
    }

    if(!$error){
        if ($_POST['f_user'] == '475692394027') {
            $data = array(
                'EMAIL' => $_POST['f_email']
            );

            $group_vallarta_lifestyles = 'Vallarta Lifestyles Weekly Newsletter (English)';

            mc_update_subscription(MAILCHIMP_API_KEY, MAILCHIMP_VLPG_LIST, $group_vallarta_lifestyles, $data);

            //send notification of users suscribed to newsletter
            // update the existing contact if address already existed
            $body = '';
            $body .= "<p>New user suscribed to newsletter from VLMG website</p><br>";
            $body .= "<b>".'Email'.":</b> " . htmlspecialchars($_POST['f_email']) . '<br>';
            $body .= "--------------------------------------------------------- <br>";
            

            $mg = Mailgun::create('key-3e0cde9342a2a366578641c36660720d');
            $mg->messages()->send('m.vlmg.mx', [
              'from'    => 'VLMG Website <no-reply@vlmg.mx>',
              'to'      => 'info@sbpvr.com',
              'bcc'      => 'jose@vallartalifestyles.com, info@vlmg.mx',
              'subject' => 'VLMG - Newsletter',
              'html'    => $body
            ]);

        }
    }

    echo json_encode( array('success' => true) );
    exit;

    