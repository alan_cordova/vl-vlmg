<?php 

function mc_update_subscription($api_key, $list_id, $group_name = false, $data = array(), $remove_group = false){

    $merge_vars = array(
        //'FNAME'=> $data['FNAME'],
        //'LNAME'=> $data['LNAME'],
        //'MMERGE3' => $data['COMPANY'],
        'GROUPINGS' => array()
    );

    $member = mailchimp_get_member($api_key, $list_id, $data['EMAIL']);

    //prepare groups
    if($group_name != false){
        if($member != false){
            foreach($member['merges']['GROUPINGS'] as $k=>$v){
                $groups_interested = array();
                foreach($v['groups'] as $group_data){
                    if($group_name == $group_data['name']){
                        if(!$remove_group){
                            $groups_interested[] = $group_data['name'];
                        }
                    }else{
                        if($group_data['interested'] == 1){
                            $groups_interested[] = $group_data['name'];
                        }
                    }
                }
                
                $merge_vars['GROUPINGS'][] = array(
                    'id' => $v['id'],
                    'groups' => $groups_interested
                );
            }
        }else{
            //default grouping and group id
            $merge_vars['GROUPINGS'][0] = array(
                'id' => MAILCHIMP_GROUPING_ID_PREFERENCES,
                'groups' => array($group_name)
            );
        }
    }

    $MailChimp = new \Drewm\MailChimp($api_key);   

    $data = array(
        'id'                => $list_id,
        'email'             => array('email'=>$data['EMAIL']),
        'merge_vars'        => $merge_vars,
        'double_optin'      => false,
        'update_existing'   => true,
        'send_welcome'      => true //($member == false) ? true : false,
    );

    $data['replace_interests'] = ($group_name != false) ? true : false;

    //attempt to register/update subscription member
    $result = $MailChimp->call('lists/subscribe', $data);

    if(isset($result['leid'])){
        return true;
    }

    return false;

}

/**
 * Get Mailchimp member data; Return false if nothing found
 */
function mailchimp_get_member($api_key, $list_id, $email){
    $MailChimp = new \Drewm\MailChimp($api_key);

    $result = $MailChimp->call('/helper/search-members', array(
        'query'             => $email,
        'id'                => $list_id,
    ));

    if($result['exact_matches']['total'] > 0){
        foreach($result['exact_matches']['members'] as $member){
            if($email == $member['email']){
                $member['groups_interested'] = array();
                foreach($member['merges']['GROUPINGS'][0]['groups'] as $groups){
                  if($groups['interested']){
                      $member['groups_interested'][0][] = $groups['name'];
                  }
                }

                return $member;
            }
        }
    }

    return false;
}
