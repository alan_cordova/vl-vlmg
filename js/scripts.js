var Webflow = Webflow || [];
Webflow.push(function() {

    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window['safari'] || safari.pushNotification);
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    var viewport;



    function get_viewport() {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        return {
            w: w,
            h: h
        };
    }

    function scroll_to_pos(top, time, extra_offset) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (top + extra_offset)
        }, time);
    }

    function scroll_to(elem, time, extra_offset) {
        time = (typeof time == "undefined") ? 300 : time;
        extra_offset = (typeof extra_offset == "undefined") ? 0 : extra_offset;
        $('html, body').stop(true, true).animate({
            scrollTop: (elem.offset().top + extra_offset)
        }, time);
    }



    Webflow.resize.on(function() {
        viewport = get_viewport();

        process_animation_1();

        process_publication_1();

        safari_flex_height_fix();
    });

    
    Webflow.scroll.on(function(e) {
    });

    function process_animation_1(){
        if(viewport.w > 767){
            proccess_animation_init();
        }else{
            $(".feature-selected").each(function(){
                $(this).css('opacity', '');
                $(this).css('display', '');
            });
        }
    }

    function process_publication_1(){
        if(viewport.w > 767){
            proccess_publication_init();
        }else{
            $(".publication-item-link").each(function(){
                $(this).css('opacity', '');
                $(this).css('display', '');
            });
        }
    }   


    var animating = false;
    $(".feature-option").click(function(){

        var index = $(this).index();
        var feature = $(".feature-selected").eq(index);
        if(!animating && !feature.hasClass('active')){
            animating = true;
            $(".feature-selected").each(function(){
                if(!$(this).hasClass('active')){
                    $(this).css('opacity', '1');
                    $(this).css('display', 'none');
                }
            });

            var current = $(".feature-selected.active");
            current.stop(true, true).animate({ opacity: 0 }, 300, function(){
                current.css('opacity', '0');
                current.css('display', 'none');
                current.removeClass('active');
    
                feature.css('opacity', '0');
                feature.show();
                feature.stop(true, true).animate({opacity: 1}, 300, function(){
                    $(this).addClass('active');
                    animating = false;
                });
            });
        }
    });
    function proccess_animation_init(){
        $(".feature-selected.active").css('opacity', 0);
        $(".feature-selected.active").stop(true, true).animate({opacity: 1}, 300);
        $(".feature-selected.active").css('display','block');
    }


    var animating_publications = false;
    $(".publication-item-link").click(function(e){
        if(viewport.w > 767){
            e.preventDefault();
        }

        var link = $(this);
        var index = $(this).index();
        var item = $(".publication-content-item").eq(index);
        if(!animating_publications && !item.hasClass('active')){
            animating_publications = true;
            $(".publication-content-item").each(function(){
                if(!$(this).hasClass('active')){
                    $(this).css('opacity', '1');
                    $(this).css('display', 'none');
                }
            });

            var current = $(".publication-content-item.active");
            current.stop(true, true).animate({ opacity: 0 }, 300, function(){
                current.css('opacity', '0');
                current.css('display', 'none');
                current.removeClass('active');
    
                item.css('opacity', '0');
                item.css('display', 'flex');
                item.stop(true, true).animate({opacity: 1}, 300, function(){
                    $(this).addClass('active');
                    animating_publications = false;
                });

                $(".publication-item-link").removeClass('enabled');
                link.addClass('enabled');
            });
        }
    });
    function proccess_publication_init(){
        $(".publication-content-item.active").css('opacity', 0);
        $(".publication-content-item.active").stop(true, true).animate({opacity: 1}, 300);
        $(".publication-content-item.active").css('display','flex');
    }



    /*** contact section ***/
    function form_in_process($btn, init){
        init = (typeof init == "undefined") ? false : init;
        var in_process = $btn.data('in-process');
        var dots = 0;
        var text = '';

        if(typeof in_process == "undefined" || init == true){
            $btn.data('in-process', '1');
            $btn.data('dots', '1');

            if(!$btn.data('original-value')){ //set original value for first time
                $btn.data('original-value', $btn.val());
            }
            in_process = 1;
        }

        in_process = parseInt(in_process);
        if(in_process == 1){
            dots = parseInt($btn.data('dots'));
            text = $btn.data('original-value');
            for(var i = 0; i < dots; i++){
                text += '.';
            }

            dots++;
            dots = (dots > 3) ? 1 : dots;

            $btn.data('dots', dots);
            $btn.val(text);
            setTimeout(function(){
                form_in_process($btn);
            }, 500);
        }
        
    }
    function form_stop_process($btn){
        $btn.data('in-process', '0');
        $btn.data('dots', '1');
        $btn.val($btn.data('original-value'));
    }

    // contact form
    var sending_contact_form = false;
    $("#contact_form").submit(function(e){
        e.preventDefault();
        var form = $(this);

        if(!sending_contact_form){
            var btn_process = form.find('.form-footer-submit');
            form_in_process(btn_process, true);

            sending_contact_form = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-block').find(".w-form-done").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.closest('.form-block').find('.w-form-done').fadeIn();
                }
                form_stop_process(btn_process);
                sending_contact_form = false;
                form.trigger("reset");
            },'json');
        }
    });
    if($("#contact_form").length > 0){
        setTimeout(function(){
            $("#contact_form").find('.cmd').val('dsasdahghj211');
        },1500);
    }


    var sending_suscribe_form = false;
    $("#newsletter_form").submit(function(e){
        e.preventDefault();
        var form = $(this);

        if(!sending_suscribe_form){
            var btn_process = form.find('.form-submit-newsletter');
            form_in_process(btn_process, true);

            sending_suscribe_form = true;
            var action = $(this).attr('action');
            var req = $(this).serializeArray();

            form.closest('.form-block').find(".w-form-done").hide();
            $.post(action, req, function(data){
                if(data.success){
                    form.closest('.form-block').find('.w-form-done').fadeIn();
                }
                form_stop_process(btn_process);
                sending_contact_form = false;
                form.trigger("reset");
            },'json');
        }
    });
    if($("#newsletter_form").length > 0){
        $("#newsletter_form").find('#f_user').val('475692394027');
    }


    function safari_flex_height_fix(){
        if(isSafari && $(".flex-height-fix").length > 0){
            $(".flex-height-fix").each(function(){
                var flex = $(this);
                var height = 0;
                flex.children().each(function(){
                    $(this).css('height', '');
                });
                
                if(viewport.w > 991){
                    flex.children().each(function(){
                        var children = $(this);
                        if(children.outerHeight() > height){
                            height = children.outerHeight();
                        }
                    });

                    flex.children().each(function(){
                        var children = $(this);
                        children.outerHeight( height );
                    });
                }
            });
        }
    }


});